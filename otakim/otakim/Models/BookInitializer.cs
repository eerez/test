﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace otakim.Models
{
    public class BookInitializer : DropCreateDatabaseIfModelChanges<BookContext>
    {
        protected override void Seed(BookContext context)
        {
            var books = new List<Book>
            {
                new Book{ BookPrice = 12, BookName="abc" },
                new Book{ BookPrice = 65, BookName="def" }
            };
            foreach (var i in books){
                context.Books.Add(i);
            }
            context.SaveChanges();
            
        }

    }
}