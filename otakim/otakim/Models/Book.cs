﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace otakim.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public int BookPrice { get; set; }
        public string BookName { get; set; }
    }
}